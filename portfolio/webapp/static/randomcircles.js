
function randomWallPoint(screenWidth, screenHeight, radius) {

    var verticalOrHorizontal = Math.round(Math.random());
    var StartOrEnd = Math.round(Math.random());

    var x = 0;
    var y = 0;

    if (verticalOrHorizontal === 0) {
        if (StartOrEnd === 0) {
            x = Math.floor(Math.random() * (screenWidth - radius - radius)) + radius;
        } else if (StartOrEnd === 1) {
            x = Math.floor(Math.random() * (screenWidth - radius - radius)) + radius;
            y = screenHeight - radius;
        }
    } else if (verticalOrHorizontal === 1) {
        if (StartOrEnd === 0) {
            y = Math.floor(Math.random() * (screenHeight - radius - radius)) + radius;
        } else if (StartOrEnd === 1) {
            x = screenWidth - radius;
            y = Math.floor(Math.random() * (screenHeight - radius - radius)) + radius;
        }
    }

    return {'x': x, 'y': y};
}


function randomWallPoints(n, screenWidth, screenHeight, radius) {
    var points = [];

    for (var i=0; i<n; i++) {
        points.push(randomWallPoint(screenWidth, screenHeight, radius));
    }

    return points;
}


function intersectingPoint(a1, a2, b1, b2) {
    var ricoA = (a2['y'] - a1['y']) / (a2['x'] - a1['x']);
    var ricoB = (b2['y'] - b1['y']) / (b2['x'] - b1['x']);

    var constantA = a1['y'] - (ricoA * a1['x']);
    var constantB = b1['y'] - (ricoB * b1['x']);

    var x = (constantB - constantA) / (ricoA - ricoB);
    var y = (ricoA * x) + constantA;

    return {'x': x, 'y': y};
}

function setInitialPositions(elementIds) {

    var elements = [];

    for (var i=0; i<elementIds.length; i++) {
        var radius = anime.random(70, 150);
        var point = randomWallPoint(window.innerWidth, window.innerHeight, radius);

        elements.push({
            'radius': radius,
            'position': point
        });

        anime({
            'targets': elementIds[i],
            'width': radius,
            'height': radius,
            'translateX': [0, point['x']],
            'translateY': [0, point['y']],
            'duration': 200
        });
    }
    return elements;

}

function distanceBetween(pointA, pointB) {

    var xDistance = Math.abs(pointB['x'] - pointA['x']);
    var yDistance = Math.abs(pointB['y'] - pointA['y']);
    var distance = Math.sqrt(
        Math.pow(xDistance, 2) + Math.pow(yDistance, 2)
    );

    return distance;
}

function isValidIntersection(intersection, boundaries) {
    if (intersection['x'] >= boundaries['minX'] &&
        intersection['x'] <= boundaries['maxX'] &&
        intersection['y'] >= boundaries['minY'] &&
        intersection['y'] <= boundaries['maxY']) {
        
        return true;
    } else {
        return false;
    }
}

function byDistanceTo(point, a, b) {

    var distanceToA = distanceBetween(point, a);
    var distanceToB = distanceBetween(point, b);

    if (distanceToA < distanceToB) { return -1; }
    else if (distanceToA === distanceToB) { return 0; }
    else if (distanceToA > distanceToB) { return 1; }

}

function randomPath(n, screenWidth, screenHeight, element) {

    var garnet1 = {'x': (screenWidth / 2), 'y': 100-100};
    var garnet2 = {'x': (screenWidth / 2) - 300 - 150, 'y': 560+75};
    var garnet3 = {'x': (screenWidth / 2) + 320 + 75, 'y': 600+100};

    var color1 = '#aaa';
    var color2 = '#d22626'
    var bg1 = null;
    if (element['radius'] <= 50) {
        bg1 = 'url("/static/logo-garnet-afrika-round-50.png") no-repeat center';
    } else if (element['radius'] <= 100) {
        bg1 = 'url("/static/logo-garnet-afrika-round-100.png") no-repeat center';
    } else {
        bg1 = 'url("/static/logo-garnet-afrika-round-150.png") no-repeat center';
    }
    var radius = element['radius']
    var currentPoint = element['position'];
    var boundaries = {
        'minX': garnet2['x'],
        'maxX': garnet3['x'],
        'minY': garnet1['y'],
        'maxY': garnet3['y']
    };

    var randomPoints = randomWallPoints(n, screenWidth, screenHeight, radius);
    var path = [];

    for (var i=0; i<n; i++) {
        var randomPoint = randomPoints[i];

        var intersections = [
            intersectingPoint(currentPoint, randomPoint, garnet1, garnet2),
            intersectingPoint(currentPoint, randomPoint, garnet1, garnet3),
            intersectingPoint(currentPoint, randomPoint, garnet2, garnet3)
        ]

        var validIntersections = [];

        for (var k=0; k<intersections.length; k++) {
            if (isValidIntersection(intersections[k], boundaries)) {
                validIntersections.push(intersections[k]);
            }
        }
        validIntersections.sort(function(a, b) { return byDistanceTo(currentPoint, a, b); });

        for (var j=0; j<validIntersections.length; j++) {
            var color = j === 0 ? color1 : color2;
            var bg = j === 0 ? null : bg1;

            var distance = distanceBetween(currentPoint, validIntersections[j]);

            path.push({
                'from': currentPoint,
                'to': validIntersections[j],
                'distance': distance,
                'color': color,
                'bg': bg
            });

            currentPoint = validIntersections[j];
        }

        path.push({
            'from': currentPoint,
            'to': randomPoint,
            'distance': distanceBetween(currentPoint, randomPoint),
            'color': color1,
            'bg': null
        });

        currentPoint = randomPoint;
    }

    return path;

}

function animationFrom(pathChunk, elementId) {

    var speed = anime.random(20, 25);

    if (pathChunk['bg'] == null) {
        return {
            'targets': elementId,
            'translateX': [pathChunk['from']['x'], pathChunk['to']['x']],
            'translateY': [pathChunk['from']['y'], pathChunk['to']['y']],
            'background': {
                'value': pathChunk['color'],
                'duration': 200
            },
            'duration': pathChunk['distance'] * speed,
            'easing': 'linear',
            'autoplay': false
        }
    } else {
        return {
            'targets': elementId,
            'translateX': [pathChunk['from']['x'], pathChunk['to']['x']],
            'translateY': [pathChunk['from']['y'], pathChunk['to']['y']],
            'background': {
                'value': pathChunk['bg'],
                'duration': 200
            },
            'duration': pathChunk['distance'] * speed,
            'easing': 'linear',
            'autoplay': false
        }
    }

}

function animateCircle(elementId, screenWidth, screenHeight, element) {

    var timeline = anime.timeline({loop: false});
    var path = randomPath(20, screenWidth, screenHeight, element);
    
    for (var i=0; i<path.length; i++) {
        var animation = animationFrom(path[i], elementId);
        timeline.add(animation);
    }
    timeline.finished.then(function() {
        animateCircle(elementId, screenWidth, screenHeight, element);
    });
}

function setGarnets() {
    var screenWidth = window.innerWidth;
    var screenHeight = window.innerHeight;

    var garnets = [];
    var logo = {};
    var dropdown = {};

    if (screenWidth < 700) {
        garnets.push({'x': (screenWidth / 2) - 75, 'y': 250});
        garnets.push({'x': (screenWidth / 2) - 75, 'y': 450});
        garnets.push({'x': (screenWidth / 2) - 75, 'y': 650});
        logo['x'] = (screenWidth / 2) - 225;
        logo['y'] = 20;
        dropdown['x'] = (screenWidth / 2) - 230;
        dropdown['y'] = 450;
    } else {
        garnets.push({'x': (screenWidth / 2) - 75, 'y': 100});
        garnets.push({'x': (screenWidth / 2) - 75 - 300, 'y': 560});
        garnets.push({'x': (screenWidth / 2) - 75 + 320, 'y': 600});
        logo['x'] = (screenWidth / 2) - 225;
        logo['y'] = (screenHeight / 2) - 75;
        dropdown['x'] = (screenWidth / 2) - 220;
        dropdown['y'] = 580;
    }

    anime({
        'targets': '#garnet1',
        'translateX': [0, garnets[0]['x']],
        'translateY': [0, garnets[0]['y']],
        'duration': 500
    });
    anime({
        'targets': '#garnet2',
        'translateX': [0, garnets[1]['x']],
        'translateY': [0, garnets[1]['y']],
        'duration': 500
    });
    anime({
        'targets': '#garnet3',
        'translateX': [0, garnets[2]['x']],
        'translateY': [0, garnets[2]['y']],
        'duration': 500
    });
    anime({
        'targets': '.logo',
        'translateX': [0, logo['x']],
        'translateY': [0, logo['y']]
    });
    anime({
        'targets': '.dropdown',
        'translateX': dropdown['x'],
        'translateY': dropdown['y'],
        'duration': 200
    })

    document.querySelector('#garnet2').addEventListener('click', function(event) {
        console.log('doing');

        var opacity = document.querySelector('.dropdown').style.opacity;

        if (opacity == 0) {
            document.querySelector('.dropdown').style.opacity = 1;
            document.querySelector('.dropdown').style.visibility = 'visible';
        }
        else {
            document.querySelector('.dropdown').style.opacity = 0;
            document.querySelector('.dropdown').style.visibility = 'hidden';
        }
    });
}

function animate() {

    elementIds = [
        '#circle1',
        '#circle2',
        '#circle3',
        '#circle4',
        '#circle5',
        '#circle6',
        '#circle7',
        '#circle8',
        '#circle9',
        '#circle10',
        '#circle11',
        '#circle12'
    ]

    var screenWidth = window.innerWidth;
    var screenHeight = window.innerHeight;
    var elements = setInitialPositions(elementIds);

    for (var i=0; i<elementIds.length; i++) {
        animateCircle(elementIds[i], screenWidth, screenHeight, elements[i]);
    }

}
setGarnets();
animate();