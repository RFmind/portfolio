from django.shortcuts import render
from .models import Service, Milestone, Project

def garnet_view(request):
    return render(request, 'primagarnet.html')

def garnet_profile_view(request):
    return render(request, 'profilepage.html')

def project_view(request, **kwargs):

    if 'name' in kwargs:
        name = ' '.join(kwargs.get('name').split('-')).title()
        project = Project.objects.get(name=name)

        if project is not None:
            return render(request, 'project.html', {'project': project})

def process_view(request, **kwargs):

    if 'name' in kwargs:
        services = Service.objects.all()
        return render(request, 'process.html', {'services': services})

def index_view(request):

    setup_data()

    context = {
        'url_freelancer': 'http://freelancer.com/u/RFmind/',
        'services': Service.objects.all(),
        'milestones': Milestone.objects.all(),
        'projects': Project.objects.all(),
        'quotes': [
            "Ruslan is a top notch python developer. He is very professional, detailed and his \
                skills are really advanced. he jumped into the project quickly and finished as required \
                to be on deadline. I am really grateful for his help. His communcation was superve and \
                gives me assistance through out this project, giving me updates every single day what \
                he was working on. He is truely a gem on freelancer.com Highly recommended for software \
                development project and high class coding along side document.",
            "Ruslan is a truly a gem to work with. He is a great python developer alongside his skills \
                with selenium are just into the next level. He completed my first project with such ease \
                and delivered what he said on time. His subject knowledge is great in data science and he \
                knows what is doing. Once he starts working on a project, he will give you the roadmap that \
                how he is going to proceed and also gives positives pieces of advice to follow and succeed. \
                Highly recommended for anybody :) will hire again :) Thanks !"
        ]
    }

    return render(request, 'index.html', context)

def setup_data():

    Project.objects.all().delete()
    Service.objects.all().delete()
    Milestone.objects.all().delete()

    Project.objects.create(
        name='Country Reports',
        short_description='For a Belgian company, I created a set of weather and currency reports per country.',
        long_description='The application integrates with the restcountries, openweathermap \
            and fixer APIs for up-to-date data',
        tech_stack='Django, PostgreSQL, requests, flexible CSS grid',
        url='/projects/country-reports',
        image_url='country-reports.png',
        service_name='Web application'
    )
    Project.objects.create(
        name='Library Management System',
        short_description='For a client in India, I created a desktop application for \
            managing a library.',
        long_description='The final application includes authentication, user management, \
            borrowing system, fee calculation and inventory management.',
        tech_stack='Python, Tkinter, PostgreSQL',
        url='/projects/library-management-system',
        image_url='library-prototype.png',
        service_name='Desktop application'
    )
    Project.objects.create(
        name='Pizza Restaurant Website',
        short_description='A clean and simple website for a conceptual restaurant; Bene Italia',
        long_description='Fully responsive, interactive and clean website. The final version \
             comes equiped with a small admin area.',
        tech_stack='Flask, Jinja2, HTML5, Mongodb',
        url='/projects/pizza-restaurant-website',
        image_url='bene-italia.jpg',
        service_name='Web application'
    )
    Project.objects.create(
        name='Football Data Scraper',
        short_description='A medium-sized project for an European client to scrape data from \
            whoscored.com',
        long_description='The core is built with Selenium and Beautifulsoup for scraping raw \
            data. After some cleaning, the data is loaded into mongodb. This dataset is then used \
            in another analysis project.',
        tech_stack='Selenium, Beautifulsoup, Mongodb',
        url='/projects/football-data-scraper',
        image_url='whoscored.png',
        service_name='Web scraper'
    )
    Service.objects.create(
        name='Web development',
        description='I will design the frontend and code the backend with all the necessary \
            integrations. Every project has its unique message and purpose. Details are \
            important! I have worked with all the major backend frameworks.'
    )
    Service.objects.create(
        name='Web scraping',
        description='Everything from a simple spider to a sophisticated custom framework is \
            possible! My go-to tools are Beautifulsoup, Selenium, Requests and Scrapy libraries.'
    )
    Service.objects.create(
        name='Desktop applications',
        description='I can create powerful and performant desktop applications for \
            multiple platforms. I have worked with PyQt, Tkinter, WxWidgets and Kivy libraries.'
    )
    Service.objects.create(
        name='Automation',
        description='I can provide tools for automating your workflow. I can also deliver \
            a fully automated environment with zero configuration on your part. Some tools \
            I use are Docker, Vagrant and Ansible.'
    )
    Milestone.objects.create(
        name='Initial meeting',
        number=0,
        description='A flexible meeting with a simple plan. We discuss some details about \
            your project. We also create a rough estimation of the budget and the time-frame.'
    )
    Milestone.objects.create(
        name='Prototype',
        number=1,
        description='I deliver the very first working and tested version; The Prototype. At this point we have a clear idea about the product and the direction we are moving.'
    )
    Milestone.objects.create(
        name='Roadmap',
        number=2,
        description='Together with The Prototype I propose a roadmap. This is a series of \
            milestones that divide the project in multiple parts. This will distribute the \
            budget and will ensure a predictable delivery of functionality.'
    )
    Milestone.objects.create(
        name='Documentation',
        number=3,
        description='Every version will be documented and I will provide coaching for the \
            relevant parts if needed. Everyone should understand what is going on and why.'
    )
