from django.db import models

class Service(models.Model):

    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)


class Milestone(models.Model):

    name = models.CharField(max_length=200)
    number = models.IntegerField()
    description = models.CharField(max_length=500)


class Project(models.Model):

    name = models.CharField(max_length=200)
    short_description = models.CharField(max_length=300)
    long_description = models.CharField(max_length=600)
    url = models.CharField(max_length=200)
    tech_stack = models.CharField(max_length=30)
    image_url = models.CharField(max_length=200)
    service_name = models.CharField(max_length=200)
